#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rollDice(void);

int main(void) {
    enum Struct {CONTINUE, WIN, LOSE};

    unsigned int sum = 0;
    unsigned int point;
    enum Struct gameStatus = CONTINUE;

    srand( time(NULL) );

    sum = rollDice();

    switch (sum) {
        case 7:
        case 11:
            gameStatus = WIN;
            break;
        case 1:
        case 2:
        case 12:
            gameStatus = LOSE;
            break;
        default:
            point = sum;
            printf("Point is %d\n", point);
            break;
    }

    while (CONTINUE == gameStatus) {
        sum = rollDice();
        if (sum == point) {
            gameStatus = WIN;
        } else if (sum == 7) {
            gameStatus = LOSE;
        }
    }

    if (gameStatus == WIN) {
        puts("Player wins");
    } else {
        puts("Player loses");
    }

    return 0;
}

int rollDice(void) {
    int dice1 = 0;
    int dice2 = 0;
    int sumDice = 0;

    dice1 = 1 + rand() % 6;
    dice2 = 1 + rand() % 6;
    sumDice = dice1 + dice2;

    printf("Player rolled %d + %d = %d\n", dice1, dice2, sumDice);
    return sumDice;
}

