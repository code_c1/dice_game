/*
How it will be work in the future?

The Game output statistic information in file.
Statistic read info from file and calculate metrics.

*/

#include <stdio.h>
#include <stdlib.h>

#define LENGTH 100
#define BASE 9

void fill_array(unsigned int a[]);
void output(unsigned int a[]);
void mean(const unsigned int a[]);
void bubble_sort(unsigned int a[]);
void median(unsigned int a[]);
void mode(const unsigned int a[]);
unsigned int find_max(const unsigned int a[], int len);


int main(void){
    unsigned int arr[LENGTH];

    fill_array(arr);
    printf("%s", "Start sequence:\n");
    output(arr);

    mean(arr);
    median(arr);
    mode(arr);
    return 0;
}

void fill_array(unsigned int a[]) {
    size_t i;
    for(i = 0; i < LENGTH; i++){
        a[i] = rand() % BASE + 1;
    }
}

void output(unsigned int a[]) {
    size_t i;
    for(i = 0; i < LENGTH; i++) {
        (i == LENGTH - 1 || (i + 1) % 20 == 0) ? printf("%2u\n", a[i]) : printf("%2u ", a[i]);
    }
}

void mean(const unsigned int a[]) {
    size_t i;
    size_t sum = 0;
    double mean_value;
    for(i = 0; i < LENGTH; i++){
        sum += a[i];
    }
    mean_value = (double) sum / LENGTH;
    printf("Mean value of sequence: %lf\n", mean_value);
}


void median(unsigned int a[]) {
    printf("%s", "\nSorted sequence:\n");
    bubble_sort(a);
    output(a);
    float median_value;
    int index = LENGTH / 2;
    if (LENGTH % 2 == 0) {
        median_value = (a[index - 1] + a[index]) / 2.0;
    } else {
        median_value = a[index];
    }
    printf("Median value of sequence: %f\n", median_value);
}


void bubble_sort(unsigned int a[]){
    size_t i, j;
    int temp;
    for (i = 1; i < LENGTH; i++){
        for(j = i; j > 0; j--){
            if(a[j] < a[j - 1]){
                temp = a[j - 1];
                a[j - 1] = a[j];
                a[j] = temp;
            }
        }
    }
}

void mode(const unsigned int a[]) {
    unsigned int freq[10] = {0};
    size_t i;
    unsigned int mode_freq;
    for(i = 0; i < LENGTH; i++) {
        freq[a[i]]++;
    }
    printf("\n%5s%11s\t%s\n", "Value", "Frequence", "Histogram");
    for(i = 1; i < 10; i++) {
        printf("%5lu%11u\t", i, freq[i]);
        for(size_t j = 0; j < freq[i]; j++){
            printf("%c", '@');
        }
        puts("");
    }
    mode_freq = find_max(freq, 10);
    printf("%s", "Mode:");
    for(i = 0; i < 10; i++) {
        if (freq[i] == mode_freq) {
            printf(" %lu", i);
        }
    }
    printf("\nEach of them occured %d times\n", mode_freq);
}

unsigned int find_max(const unsigned int a[], int len) {
    unsigned int maximum = a[0];
    int i;
    for(i = 0; i < len; i++){
        if (a[i] > maximum) {
            maximum = a[i];
        }
    }
    return maximum;
}